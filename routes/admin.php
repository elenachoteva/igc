<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

/****** ADMIN *******/

Route::prefix('igc-admin')->group(function () {
    Route::get('login', 'Admin\AuthController@showLoginForm')->name('admin.loginForm');
    Route::post('login', 'Admin\AuthController@login')->name('admin.login');
    Route::post('logout', 'Admin\AuthController@logout')->name('admin.logout');

    Route::group(['middleware' => 'admin.auth'], function() {
        Route::get('pages', 'Admin\PageController@index')->name('admin.pages.index');
        Route::get('pages/create', 'Admin\PageController@create')->name('admin.pages.create');
        Route::post('pages', 'Admin\PageController@store')->name('admin.pages.store');
        Route::get('pages/{id}', 'Admin\PageController@edit')->name('admin.pages.edit');
        Route::put('pages/{id}', 'Admin\PageController@update')->name('admin.pages.update');
        Route::delete('pages/{id}', 'Admin\PageController@destroy')->name('admin.pages.destroy');
        Route::resource('categories', 'Admin\CategoryController')->except(['show']);
        Route::resource('cities', 'Admin\CityController')->except(['show']);
        Route::resource('advertisements', 'Admin\AdvertisementController')->except(['show']);
        Route::delete('advertisements/{advertisement}/image/{image}', 'Admin\AdvertisementController@deleteImage');
    });
});