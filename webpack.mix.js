const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/admin.scss', 'public/admin/css')
    .sass('resources/sass/app.scss', 'public/css')
    .js([
        'resources/js/bootstrap.js'
    ], 'public/admin/js/admin.js')
    .js([
        'resources/js/custom.js'
    ], 'public/admin/js/custom.js')
    .js([
        'node_modules/tinymce/tinymce.min.js'
    ], 'public/admin/js/tinymce.js')
    .js([
        'node_modules/jquery.repeater/src/repeater.js'
    ], 'public/admin/js/jquery.repeater.js')
    .copy('node_modules/tinymce/skins', 'public/admin/js/skins')
    .copy('node_modules/tinymce/themes', 'public/admin/js/themes')
    .copy('node_modules/tinymce/plugins', 'public/admin/js/plugins');
