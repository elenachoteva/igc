var base_url = base_path;

$(document).ready(function () {
    $('.ad_image__delete').on('click', function () {
        var confirmWindow = confirm('Сигурни ли сте, че искате да изтриете тази снимка?');
        if (confirmWindow == true) {
            var ad_id = $(this).attr('data-ad-id');
            var image_id = $(this).attr('data-image-id');
            var row_to_delete = $(this).parent().parent();

            $.ajax({
                type: "DELETE",
                url: base_url + '/igc-admin/advertisements/' + ad_id + '/image/' + image_id,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    if (response.message) {
                        $('<div class="alert alert-success alert-dismissible" role="alert">Успешно изтрито изображение<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').insertBefore('.product_image__container');
                        row_to_delete.remove();
                    }
                },
                error: function (errors) {
                    var server_errors = $.parseJSON(errors.responseText);
                    if (server_errors.error) {
                        $('<div class="alert alert-danger alert-dismissible" role="alert">'+server_errors.error+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').insertBefore('.product_image__container');
                    }
                }
            });
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});