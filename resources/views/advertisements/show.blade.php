@extends('layouts.main')

@section('content')
    <div class="card p-4">
        @if(isset($adMainImage))
            <img class="card-img-igc mw-100"
                 src="{{asset('uploads/advertisements/' . $ad->id . '/' . $adMainImage->filename)}}"
                 alt="{{$ad->title}}"/>
        @endif
        <div class="card-body">
            <h5 class="card-title">{{$ad->title}}</h5>
            <p class="card-text">{!! $ad->description !!}</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Цена: {{$ad->price}}</li>
            <li class="list-group-item">Брой преглеждания: {{$ad->views_count}}</li>
            <li class="list-group-item">Изтича на: {{$ad->expires_at->format('d M Y')}}</li>
        </ul>
        @if(count($adImages) > 0)
            <div class="card-body">
                <div id="carouselAdImages" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @forelse($adImages as $key => $image)
                            <div class="carousel-item {{$key === 0 ? 'active' : ''}}">
                                <img class="d-block m-auto mw-100"
                                     src="{{asset('uploads/advertisements/' . $ad->id . '/' . $image->filename)}}"
                                     alt="">
                            </div>
                        @empty
                        @endforelse
                    </div>
                    <a class="carousel-control-prev" href="#carouselAdImages" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                            <i class="fa fa-caret-left fa-3x"></i>
                        </span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselAdImages" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                            <i class="fa fa-caret-right fa-3x"></i>
                        </span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('admin-js')
    <script>
        $(document).ready(function () {
            $('.carousel').carousel();
        });
    </script>
@endsection