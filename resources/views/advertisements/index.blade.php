@extends('layouts.main')

@section('content')
    @if(Session::has('error_message'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ Session::get('error_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <table class="table ads">
        <thead class="thead-dark">
            <tr>
                <th>Заглавие</th>
                <th>Цена</th>
                <th>Брой преглеждания</th>
                <th>Създадена</th>
                <th>Виж повече</th>
            </tr>
        </thead>
        <tbody>
            @forelse($ads as $ad)
                <tr>
                    <td>{{$ad->title}}</td>
                    <td>{{$ad->price}}</td>
                    <td>{{$ad->views_count}}</td>
                    <td>{{$ad->created_at->format('d M Y')}}</td>
                    <td>
                        <a class="edit-button btn btn-outline-primary"
                           href="{{ route('showAdvertisement', ['advertisement' => $ad->id]) }}"><i class="fa fa-eye mr-0"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Няма намерени обяви</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection