@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Страници</h4>
                    <div class="page-description d-flex mb-5">
                        <div class="col-12 col-md-12 text-right">
                            <a class="btn btn-success" href="{{route('admin.pages.create')}}" title="Нова страница"><i
                                        class="icon-plus mr-2"></i>Нова страница</a>
                        </div>
                    </div>
                    <div id="order-listing_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-lg-12 table-responsive">
                                <table class="table dataTable table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Заглавие<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Slug<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Текст<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Създадена<i class="mdi mdi-chevron-down"></i></th>
                                        <th class="text-center">Промени</th>
                                        <th class="text-center">Изтрий</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($pages as $page)
                                        <tr>
                                            <td>{{ $page->title }}</td>
                                            <td>{{ $page->slug }}</td>
                                            <td>{{ $page->text }}</td>
                                            <td>{{ $page->created_at->format('d M Y') }}</td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <a class="edit-button btn btn-outline-primary"
                                                   href="{{route('admin.pages.edit', ['id' => $page->id])}}"
                                                   title="Промени"><i class="icon-pencil menu-icon mr-0"></i></a>
                                            </td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <form method="POST"
                                                      action="{{route('admin.pages.destroy', ['id' => $page->id])}}"
                                                      class="forms-sample">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit"
                                                            onclick="return confirm('Сигурни ли сте, че искате да изтриете тази страница?')"
                                                            class="delete-button btn btn-outline-danger" title="Изтрий"><i
                                                                class="icon-trash menu-icon mr-0"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">Не са намерени страници</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
