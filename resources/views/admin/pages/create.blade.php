@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('Добави нова страница')}}</h4>
                    <div class="row">
                        <div class="col-xl-12">
                            <form method="POST" action="{{route('admin.pages.store')}}"
                                  class="forms-sample mt-4" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Заглавие</label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                   name="title" value="{{old('title')}}"/>
                                            @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Текст</label>
                                        <div class="col-sm-9">
                                            <div class="row grid-margin">
                                                <div class="col-lg-12">
                                                    <textarea name="text"
                                                              id="pageText">{{old('text')}}</textarea>
                                                </div>
                                            </div>
                                            @error('text')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Slug</label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                                   name="slug" value="{{old('slug')}}"/>
                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Снимка</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <input type="file" name="filename" class="file-upload-default">
                                                <div class="input-group col-xs-12">
                                                    <input type="text" class="form-control file-upload-info {{ $errors->has('filename') ? ' is-invalid' : '' }}"
                                                           placeholder="Upload Image">
                                                    <span class="input-group-append">
                                                        <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                                                    </span>
                                                </div>
                                            </div>
                                            @error('filename')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success mr-2 mt-3">Запази</button>
                                <a href="{{route('admin.pages.index')}}" class="btn btn-light mt-3">Отмени</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('admin-js')
    <script src="{{ asset('admin/js/tinymce.js') }}"></script>
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: '#pageText',
                height: 250,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ]
            });
        });
    </script>
@endsection