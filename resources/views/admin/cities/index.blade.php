@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Населени места</h4>
                    <div class="page-description d-flex mb-5">
                        <div class="col-12 col-md-12 text-right">
                            <a class="btn btn-success" href="{{route('cities.create')}}" title="Ново населено място"><i
                                        class="icon-plus mr-2"></i>Ново населено място</a>
                        </div>
                    </div>
                    <div id="order-listing_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-lg-12 table-responsive">
                                <table class="table dataTable table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Име<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Създадена<i class="mdi mdi-chevron-down"></i></th>
                                        <th class="text-center">Промени</th>
                                        <th class="text-center">Изтрий</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($cities as $city)
                                        <tr>
                                            <td>{{ $city->title }}</td>
                                            <td>{{ $city->created_at->format('d M Y') }}</td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <a class="edit-button btn btn-outline-primary"
                                                   href="{{route('cities.edit', ['city' => $city->id])}}"
                                                   title="Промени"><i class="icon-pencil menu-icon mr-0"></i></a>
                                            </td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <form method="POST"
                                                      action="{{route('cities.destroy', ['city' => $city->id])}}"
                                                      class="forms-sample">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit"
                                                            onclick="return confirm('Сигурни ли сте, че искате да изтриете това населено място?')"
                                                            class="delete-button btn btn-outline-danger" title="Изтрий"><i
                                                                class="icon-trash menu-icon mr-0"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">Не са намерени населени места</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
