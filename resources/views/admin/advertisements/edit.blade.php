@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('Редактирай обява')}}</h4>
                    <div class="row">
                        <div class="col-xl-12">
                            <form method="POST"
                                  action="{{route('advertisements.update', ['advertisement' => $ad->id])}}"
                                  class="forms-sample mt-4" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Заглавие</label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                   name="title" value="{{ $ad->title }}"/>
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Описание</label>
                                        <div class="col-sm-9">
                                            <div class="row grid-margin">
                                                <div class="col-lg-12">
                                                    <textarea name="description"
                                                              id="adDescription">{{ $ad->description }}</textarea>
                                                </div>
                                            </div>
                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Цена</label>
                                        <div class="col-sm-9">
                                            <input type="number"
                                                   class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}"
                                                   name="price" value="{{ $ad->price }}" min="0" step="0.01"/>
                                            @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Активна обява</label>
                                        <div class="col-xl-1 col-sm-5">
                                            <div class="form-radio">
                                                <input type="radio" id="activeAd"
                                                       class="{{ $errors->has('active') ? ' is-invalid' : '' }}"
                                                       name="active"
                                                       {{$ad->active ? 'checked' : ''}}
                                                       value="1">
                                                <label for="activeAd">
                                                    Да
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xl-1 col-sm-4">
                                            <div class="form-radio">
                                                <input type="radio" id="inactiveAd" class=""
                                                       name="active"
                                                       {{!$ad->active ? 'checked' : ''}}
                                                       value="0">
                                                <label for="inactiveAd">
                                                    Не
                                                </label>
                                            </div>
                                        </div>
                                        @error('active')
                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Изтича на</label>
                                        <div class="col-sm-9">
                                            <input type="date" id="adExpiresAt"
                                                   class="form-control {{ $errors->has('expires_at') ? ' is-invalid' : '' }}"
                                                   name="expires_at" value="{{ $ad->expires_at->format('Y-m-d') }}"/>
                                            @error('expires_at')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Населени места</label>
                                        <div class="col-sm-9">
                                            @if(count($cities) > 0)
                                                <select multiple name="cities[]" class="form-control" id="adCities">
                                                    @foreach($cities as $city)
                                                        <option {{ count($ad->cities) > 0 && in_array($city->id, $ad->cities()->pluck('city_id')->toArray()) ? 'selected' : '' }} value="{{$city->id}}">{{ $city->title }}</option>
                                                    @endforeach
                                                </select>
                                                @error('cities')
                                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Текуща основна снимка</label>
                                        <div class="col-sm-9">
                                            @if(isset($adMainImage))
                                                <img class="admin-pics"
                                                     src="{{asset('uploads/advertisements/' . $ad->id . '/' . $adMainImage->filename)}}"
                                                     alt="{{$ad->title}}"/>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Основна снимка</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                <input type="file" name="main_filename" class="file-upload-default">
                                                <div class="input-group col-xs-12">
                                                    <input type="text"
                                                           class="form-control file-upload-info {{ $errors->has('main_filename') ? ' is-invalid' : '' }}"
                                                           placeholder="Upload Image">
                                                    <span class="input-group-append">
                                                        <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                                                    </span>
                                                </div>
                                            </div>
                                            @error('main_filename')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group product_image__container row">
                                        <label class="col-sm-3 col-form-label">Текущи снимки</label>
                                        <div class="col-sm-9">
                                            @forelse($adImages as $image)
                                                <div class="row mb-2">
                                                    <div class="col-sm-10"><img style="max-width: 200px;" src="{{asset('uploads/advertisements/' . $ad->id . '/' . $image->filename)}}"
                                                                               alt="{{$ad->title}}"/></div>
                                                    <div class="col-sm-2 d-flex"
                                                         style="align-items: center;justify-content: center;">
                                                        <button type="button" data-ad-id="{{$ad->id}}"
                                                                data-image-id="{{$image->id}}"
                                                                class="delete-button ad_image__delete"
                                                                title="Delete"><i class="icon-trash menu-icon"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            @empty
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Снимки</label>
                                        <div class="col-sm-9">
                                            <div class="form-inline repeater">
                                                <div data-repeater-list="images">
                                                    <div data-repeater-item class="d-flex mb-2">
                                                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                            <input type="file" name="filename"
                                                                   class="form-control form-control-sm h-auto"
                                                                   placeholder="Качи изображение">
                                                        </div>
                                                        <button data-repeater-delete type="button"
                                                                class="btn btn-danger btn-sm icon-btn ml-2">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <button data-repeater-create type="button"
                                                        class="btn btn-info btn-sm icon-btn ml-2 mb-2">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Категории</label>
                                        <div class="col-sm-9">
                                            @if(count($categories) > 0)
                                                <select multiple name="categories[]" class="form-control"
                                                        id="adCategories">
                                                    @foreach($categories as $category)
                                                        <option {{ count($ad->categories) > 0 && in_array($category->id, $ad->categories()->pluck('category_id')->toArray()) ? 'selected' : '' }} value="{{$category->id}}">{{ $category->title }}</option>
                                                    @endforeach
                                                </select>
                                                @error('categories')
                                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                @enderror
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success mr-2 mt-3">Промени</button>
                                <a href="{{route('advertisements.index')}}" class="btn btn-light mt-3">Отмени</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('admin-js')
    <script src="{{ asset('admin/js/tinymce.js') }}"></script>
    <script src="{{ asset('admin/js/file-upload.js') }}"></script>
    <script src="{{ asset('admin/js/jquery.repeater.min.js') }}"></script>
    <script src="{{ asset('admin/js/custom.js') }}"></script>
    <script>
        $(document).ready(function () {
            tinymce.init({
                selector: '#adDescription',
                height: 250,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'template paste textcolor colorpicker textpattern imagetools codesample toc help'
                ]
            });

            $('.repeater').repeater({
                show: function() {
                    $(this).slideDown();
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                isFirstItemUndeletable: true
            });
        });
    </script>
@endsection