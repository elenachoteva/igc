@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Обяви</h4>
                    <div class="page-description d-flex mb-5">
                        <div class="col-12 col-md-12 text-right">
                            <a class="btn btn-success" href="{{route('advertisements.create')}}" title="Нова обява"><i
                                        class="icon-plus mr-2"></i>Нова обява</a>
                        </div>
                    </div>
                    <div id="order-listing_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-lg-12 table-responsive">
                                <table class="table dataTable table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Заглавие<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Цена<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Брой преглеждания<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Създадена<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Изтича на<i class="mdi mdi-chevron-down"></i></th>
                                        <th class="text-center">Промени</th>
                                        <th class="text-center">Изтрий</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($ads as $ad)
                                        <tr>
                                            <td>{{ $ad->title }}</td>
                                            <td>{{ $ad->price }} лв.</td>
                                            <td>{{ $ad->views_count }}</td>
                                            <td>{{ $ad->created_at->format('d M Y') }}</td>
                                            <td>{{ $ad->expires_at->format('d M Y') }}</td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <a class="edit-button btn btn-outline-primary"
                                                   href="{{route('advertisements.edit', ['advertisement' => $ad->id])}}"
                                                   title="Промени"><i class="icon-pencil menu-icon mr-0"></i></a>
                                            </td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <form method="POST"
                                                      action="{{route('advertisements.destroy', ['advertisement' => $ad->id])}}"
                                                      class="forms-sample">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit"
                                                            onclick="return confirm('Сигурни ли сте, че искате да изтриете тази обява?')"
                                                            class="delete-button btn btn-outline-danger" title="Изтрий"><i
                                                                class="icon-trash menu-icon mr-0"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">Не са намерени обяви</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
