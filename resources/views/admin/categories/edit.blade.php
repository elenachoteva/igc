@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{__('Редактирай категория')}}</h4>
                    <div class="row">
                        <div class="col-xl-12">
                            <form method="POST" action="{{route('categories.update', ['category' => $category->id])}}"
                                  class="forms-sample mt-4">
                                @method('PUT')
                                @csrf
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Заглавие</label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}"
                                                   name="title" value="{{ $category->title }}" required/>
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Slug</label>
                                        <div class="col-sm-9">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}"
                                                   name="slug" value="{{ $category->slug }}" required/>
                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-success mr-2 mt-3">Промени</button>
                                <a href="{{route('categories.index')}}" class="btn btn-light mt-3">Отмени</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
