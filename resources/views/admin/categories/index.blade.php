@extends('admin.layouts.admin')

@section('content')
    @if(Session::has('success_message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ Session::get('success_message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="row">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Категории</h4>
                    <div class="page-description d-flex mb-5">
                        <div class="col-12 col-md-12 text-right">
                            <a class="btn btn-success" href="{{route('categories.create')}}" title="Нова категория"><i
                                        class="icon-plus mr-2"></i>Нова категория</a>
                        </div>
                    </div>
                    <div id="order-listing_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                        <div class="row">
                            <div class="col-lg-12 table-responsive">
                                <table class="table dataTable table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Заглавие<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Slug<i class="mdi mdi-chevron-down"></i></th>
                                        <th>Създадена<i class="mdi mdi-chevron-down"></i></th>
                                        <th class="text-center">Промени</th>
                                        <th class="text-center">Изтрий</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($categories as $category)
                                        <tr>
                                            <td>{{ $category->title }}</td>
                                            <td>{{ $category->slug }}</td>
                                            <td>{{ $category->created_at->format('d M Y') }}</td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <a class="edit-button btn btn-outline-primary"
                                                   href="{{route('categories.edit', ['category' => $category->id])}}"
                                                   title="Промени"><i class="icon-pencil menu-icon mr-0"></i></a>
                                            </td>
                                            <td class="jsgrid-cell jsgrid-control-field jsgrid-align-center text-center">
                                                <form method="POST"
                                                      action="{{route('categories.destroy', ['category' => $category->id])}}"
                                                      class="forms-sample">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit"
                                                            onclick="return confirm('Сигурни ли сте, че искате да изтриете тази категория?')"
                                                            class="delete-button btn btn-outline-danger" title="Изтрий"><i
                                                                class="icon-trash menu-icon mr-0"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">Не са намерени категории</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
