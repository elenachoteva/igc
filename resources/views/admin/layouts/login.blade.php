<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ 'IGC Admin Panel' }}</title>

    <!-- Styles -->
    <link href="{{ asset('admin/css/admin.css') }}" rel="stylesheet">
    <style>
        body {
            height: 100vh;
            background-color: #f1f1f1;
        }

        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 22px;
            border: 1px solid transparent;
        }

        .navbar .container {
            padding-left: 15px;
            padding-right: 15px;
            margin: 0 auto;
        }

        .navbar-header {
            float: left;
        }

        .navbar-default .navbar-collapse, .navbar-default .navbar-form {
            border-color: #d3e0e9;
        }

        .navbar-right {
            float: right !important;
            margin-right: -15px;
        }

        .navbar-nav > li {
            float: left;
        }

        .nav > li, .nav > li > a {
            display: block;
            position: relative;
        }

        .navbar-nav > li > a {
            padding-top: 14px;
            padding-bottom: 14px;
        }

        .navbar-collapse.collapse {
            display: block !important;
            height: auto !important;
            padding-bottom: 0;
            overflow: visible !important;
        }

        .navbar > .container, .navbar > .container-fluid {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-laravel navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ route('admin.loginForm') }}">
                    <img src="{{ asset('images/igc-logo.jpg') }}" width="120" alt="IGC Logo"/>
                </a>
            </div>
        </div>
    </nav>

    @yield('content')
</div>
<!-- Scripts -->
<script src="{{ asset('admin/js/admin.js') }}"></script>
</body>
</html>
