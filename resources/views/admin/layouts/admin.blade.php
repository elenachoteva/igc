<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ 'IGC Admin Panel' }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- inject:css -->
    <link href="{{ asset('admin/css/admin.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper">
            <a class="navbar-brand brand-logo" href="{{route('admin.pages.index')}}">
                <img src="{{ asset('images/igc-logo.jpg') }}" width="120" alt="IGC Logo"/>
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{route('admin.pages.index')}}">
                <img src="{{ asset('images/igc-logo.jpg') }}" width="120" alt="IGC Logo"/>
            </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav ml-lg-auto">
                <li class="nav-item dropdown d-none d-sm-block profile-img">
                    <a class="nav-link dropdown-toggle profile-image" href="#" role="button"
                       data-toggle="dropdown" aria-expanded="false">
                        <img src="{{asset('admin/images/faces-clipart/pic-4.png')}}" alt="profile-img" title="{{auth()->user()->name}}">
                        <span class="online-status online bg-success"></span>
                    </a>
                    <div class="dropdown-menu navbar-dropdown dropdown-menu-right">
                        <form action="{{route('admin.logout')}}" method="POST" class="dropdown-item">
                            @csrf
                            <button type="submit" class="dropdown-item">{{__('Изход')}}</button>
                        </form>
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center ml-auto" type="button"
                    data-toggle="offcanvas">
                <span class="icon-menu icons"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <div class="row row-offcanvas row-offcanvas-right position-relative">
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-category">
                        <span class="nav-link">СТРАНИЦИ</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.pages.index')}}">
                            <span class="menu-title">Виж всички</span>
                            <i class="icon-notebook menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item nav-category">
                        <span class="nav-link">КАТЕГОРИИ</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('categories.index')}}">
                            <span class="menu-title">Виж всички</span>
                            <i class="icon-layers menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item nav-category">
                        <span class="nav-link">НАСЕЛЕНИ МЕСТА</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('cities.index')}}">
                            <span class="menu-title">Виж всички</span>
                            <i class="icon-home menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item nav-category">
                        <span class="nav-link">ОБЯВИ</span>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('advertisements.index')}}">
                            <span class="menu-title">Виж всички</span>
                            <i class="icon-list menu-icon"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- content-wrapper ends -->
            <footer class="footer">
                <div class="container-fluid clearfix">
                    <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © {{now()->year}}</span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<script>
    window.base_path = '{{ env('APP_URL') }}';
</script>
<!-- container-scroller -->
<script src="{{ asset('admin/js/admin.js') }}"></script>
@yield('admin-js')
</body>

</html>