<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Advertisement;
use Carbon\Carbon;

class SetExpiredAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ad:make_inactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set expired advertisements to inactive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Advertisement::where('active', true)
            ->where('expires_at', '<', Carbon::now('Europe/Sofia')->startOfDay())
            ->update(['active' => false]);
    }
}
