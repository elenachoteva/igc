<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'active',
        'expires_at',
        'views_count'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dates = ['expires_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(AdvertisementImage::class)->orderBy('id', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'advertisement_categories');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cities()
    {
        return $this->belongsToMany(City::class, 'advertisement_cities');
    }

    /**
     * @param $nameInput
     */
    public function attachCategories($nameInput)
    {
        if (count($nameInput) > 0) {
            foreach ($nameInput as $value) {
                $this->categories()->attach($value, ['advertisement_id' => $this->id]);
            }
        }
    }

    /**
     * @param $nameInput
     */
    public function attachCities($nameInput)
    {
        if (count($nameInput) > 0) {
            foreach ($nameInput as $value) {
                $this->cities()->attach($value, ['advertisement_id' => $this->id]);
            }
        }
    }

    /**
     * @return mixed
     */
    public static function getActiveAds()
    {
        return self::where('active', true)->orderBy('created_at', 'DESC')->get();
    }
}
