<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $pages = Page::getPagesWithStrippedText();
        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store new page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        if ($request->hasFile('filename')) {
            $lastPage = Page::orderBy('id', 'DESC')->first();
            $lastInsertedPageId = $lastPage ? $lastPage->id + 1 : 1;
            Storage::disk('uploads')->putFileAs('uploads/pages/' . $lastInsertedPageId, $request->file('filename'), $request->file('filename')->getClientOriginalName());
        }

        $page = Page::create([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'slug' => $request->get('slug'),
            'filename' => $request->hasFile('filename') ? $request->file('filename')->getClientOriginalName() : ''
        ]);

        return redirect()->route('admin.pages.edit', ['id' => $page->id]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $page = Page::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('admin.pages.index')->withErrors($exception);
        }

        return view('admin.pages.edit', compact('page'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $page = Page::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('admin.pages.index')->withErrors($exception);
        }

        $this->validateRequest($request, $id);

        if ($request->hasFile('filename')) {
            $files = Storage::disk('uploads')->allFiles('uploads/pages/' . $page->id);
            Storage::disk('uploads')->delete($files);
            Storage::disk('uploads')->putFileAs('uploads/pages/' . $page->id, $request->file('filename'), $request->file('filename')->getClientOriginalName());
        }

        $page->update([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'slug' => $request->get('slug'),
            'filename' => $request->hasFile('filename') ? $request->file('filename')->getClientOriginalName() : $page->filename
        ]);

        return redirect()->route('admin.pages.edit', ['id' => $page->id]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $page = Page::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('admin.pages.index')->withErrors($exception);
        }

        Storage::disk('uploads')->deleteDirectory('uploads/pages/' . $id);
        $page->delete();

        return redirect()->route('admin.pages.index');
    }

    /**
     * @param $request
     * @param bool $id
     * @return mixed
     */
    private function validateRequest($request, $id = false)
    {
        $slugValidationRules = $id ? 'required|unique:pages,slug,' . $id : 'required|unique:pages,slug';
        $filenameValidationRules = $id ? 'file|mimes:jpeg,jpg,bmp,png,svg|max:10240' : 'required|file|mimes:jpeg,jpg,bmp,png,svg|max:10240';
        $validator = [
            'title' => 'required|string|max:255',
            'text' => 'string',
            'slug' => $slugValidationRules,
            'filename' => $filenameValidationRules
        ];
        return $request->validate($validator);
    }
}
