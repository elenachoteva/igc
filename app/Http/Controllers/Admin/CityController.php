<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CityController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $cities = City::orderBy('id', 'DESC')->get();
        return view('admin.cities.index', compact('cities'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cities.create');
    }

    /**
     * Store new category
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $city = City::create([
            'title' => $request->get('title')
        ]);

        return redirect()->route('cities.edit', ['city' => $city->id])->with('success_message', 'Успешно създадено населено място.');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('cities.index')->withErrors($exception);
        }

        return view('admin.cities.edit', compact('city'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('cities.index')->withErrors($exception);
        }

        $this->validateRequest($request);

        $city->update([
            'title' => $request->get('title')
        ]);

        return redirect()->route('cities.edit', ['city' => $city->id])
            ->with('success_message', 'Успешно променено населено място.');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $city = City::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('cities.index')->withErrors($exception);
        }

        $city->delete();

        return redirect()->route('cities.index');
    }

    /**
     * @param $request
     * @return mixed
     */
    private function validateRequest($request)
    {
        $validator = [
            'title' => 'required|string|max:100'
        ];
        return $request->validate($validator);
    }
}