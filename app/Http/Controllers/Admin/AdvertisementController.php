<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Advertisement;
use App\AdvertisementImage;
use App\City;
use App\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class AdvertisementController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $ads = Advertisement::orderBy('id', 'DESC')->get();
        return view('admin.advertisements.index', compact('ads'));
    }

    /**
     * Show the form for creating a new page.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::orderBy('title', 'ASC')->get();
        $categories = Category::orderBy('title', 'ASC')->get();
        return view('admin.advertisements.create', compact('cities', 'categories'));
    }

    /**
     * Store new page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $ad = Advertisement::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'active' => $request->get('active'),
            'expires_at' => Carbon::createFromFormat('Y-m-d', $request->get('expires_at'), 'Europe/Sofia')
        ]);

        if ($request->hasFile('main_filename')) {
            Storage::disk('uploads')->putFileAs('uploads/advertisements/' . $ad->id, $request->file('main_filename'), $request->file('main_filename')->getClientOriginalName());
            $ad->images()->create([
                'filename' => $request->file('main_filename')->getClientOriginalName(),
                'main_filename' => true
            ]);
        }

        if ($request->file('images') && count($request->file('images')) > 0) {
            foreach ($request->file('images') as $key => $file) {
                $uploadedFile = $request->file('images.' . $key . '.filename');
                Storage::disk('uploads')->putFileAs('uploads/advertisements/' . $ad->id, $uploadedFile, $uploadedFile->getClientOriginalName());
                $ad->images()->create([
                    'filename' => $uploadedFile->getClientOriginalName(),
                    'main_filename' => false
                ]);
            }
        }

        $ad->attachCategories($request->get('categories'));
        $ad->attachCities($request->get('cities'));

        return redirect()->route('advertisements.edit', ['advertisement' => $ad->id]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $ad = Advertisement::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('advertisements.index')->withErrors($exception);
        }

        $cities = City::orderBy('title', 'ASC')->get();
        $categories = Category::orderBy('title', 'ASC')->get();
        $adMainImage = $ad->images()->where('main_filename', true)->first();
        $adImages = $ad->images()->where('main_filename', false)->get();

        return view('admin.advertisements.edit', compact('ad', 'cities', 'categories', 'adMainImage', 'adImages'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $ad = Advertisement::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('advertisements.index')->withErrors($exception);
        }

        $this->validateRequest($request, $id);

        $ad->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'active' => $request->get('active'),
            'expires_at' => Carbon::createFromFormat('Y-m-d', $request->get('expires_at'), 'Europe/Sofia')
        ]);

        if ($request->hasFile('main_filename')) {
            $adMainImage = $ad->images()->where('main_filename', true)->first();
            Storage::disk('uploads')->delete('uploads/advertisements/' . $ad->id . '/' . $adMainImage->filename);
            Storage::disk('uploads')->putFileAs('uploads/advertisements/' . $ad->id, $request->file('main_filename'), $request->file('main_filename')->getClientOriginalName());
            $adMainImage->update([
                'filename' => $request->file('main_filename')->getClientOriginalName()
            ]);
        }

        if ($request->file('images') && count($request->file('images')) > 0) {
            foreach ($request->file('images') as $key => $file) {
                $uploadedFile = $request->file('images.' . $key . '.filename');
                Storage::disk('uploads')->putFileAs('uploads/advertisements/' . $ad->id, $uploadedFile, $uploadedFile->getClientOriginalName());
                $ad->images()->create([
                    'filename' => $uploadedFile->getClientOriginalName(),
                    'main_filename' => false
                ]);
            }
        }

        $ad->categories()->sync($request->get('categories'));
        $ad->cities()->sync($request->get('cities'));

        return redirect()->route('advertisements.edit', ['advertisement' => $ad->id]);
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $ad = Advertisement::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('advertisements.index')->withErrors($exception);
        }

        Storage::disk('uploads')->deleteDirectory('uploads/advertisements/' . $id);
        $ad->delete();

        return redirect()->route('advertisements.index');
    }

    /**
     * @param $request
     * @param bool $id
     * @return mixed
     */
    private function validateRequest($request, $id = false)
    {
        $mainFilename = $id ? 'file|mimes:jpeg,jpg,bmp,png,svg|max:10240' : 'required|file|mimes:jpeg,jpg,bmp,png,svg|max:10240';
        $validator = [
            'title' => 'required|string|max:150',
            'description' => 'string',
            'price' => 'required|numeric|min:0',
            'active' => 'required|boolean',
            'expires_at' => 'required|date|after:tomorrow',
            'main_filename' => $mainFilename,
            'categories' => 'array|required',
            'categories.*' => 'exists:categories,id',
            'cities' => 'array|required',
            'cities.*' => 'exists:cities,id',
            'images' => 'array',
            'images.*.filename' => 'file|mimes:jpeg,jpg,bmp,png,svg|max:10240'
        ];
        return $request->validate($validator);
    }

    /**
     * @param $ad
     * @param $image
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage($ad, $image)
    {
        try {
            $adFound = Advertisement::findOrFail($ad);
            $imageFound = AdvertisementImage::findOrFail($image);
        } catch (\Throwable $exception) {
            return response()->json([
                'error' => 'Не може да бъде намерена такава обява или снимка'
            ], 404);
        }

        Storage::disk('uploads')->delete('uploads/advertisements/' . $ad . '/' . $imageFound->filename);
        $imageFound->delete();

        return response()->json([
            'message' => 'success'
        ], 200);
    }
}
