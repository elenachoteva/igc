<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $categories = Category::orderBy('id', 'DESC')->get();
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store new category
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $category = Category::create([
            'title' => $request->get('title'),
            'slug' => $request->get('slug')
        ]);

        return redirect()->route('categories.edit', ['category' => $category->id])->with('success_message', 'Успешно създадена категория.');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('categories.index')->withErrors($exception);
        }

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('categories.index')->withErrors($exception);
        }

        $this->validateRequest($request, $id);

        $category->update([
            'title' => $request->get('title'),
            'slug' => $request->get('slug')
        ]);

        return redirect()->route('categories.edit', ['category' => $category->id])
            ->with('success_message', 'Успешно променена категория.');
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('categories.index')->withErrors($exception);
        }

        $category->delete();

        return redirect()->route('categories.index');
    }

    /**
     * @param $request
     * @param bool $id
     * @return mixed
     */
    private function validateRequest($request, $id = false)
    {
        $slugValidationRules = $id ? 'required|unique:categories,slug,' . $id : 'required|unique:categories,slug';
        $validator = [
            'title' => 'required|string|max:150',
            'slug' => $slugValidationRules
        ];
        return $request->validate($validator);
    }
}