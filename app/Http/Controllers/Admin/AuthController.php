<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    private $validator = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * @param Request $request
     * @return AuthController
     */
    public function login(Request $request)
    {
        if (!$this->validateLogin($request)) {
            return redirect()
                ->route('admin.loginForm')
                ->withErrors($this->validator)
                ->withInput();
        }

        if (auth()->attempt(['email' => $request->email, 'password' => $request->password, 'admin' => 1])) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request)
    {
        auth()->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }

    /**
     * Validate the user login request.
     *
     * @param Request $request
     * @return bool
     */
    private function validateLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string'
        ]);

        $user = User::where('email', $request->get('email'))->first();

        $validator->after(function ($validator) use ($user, $request) {
            if ($user === null) {
                $validator->errors()->add('email', 'Не е намерен такъв запис');
            } elseif ($user && !$user->isAdmin()) {
                $validator->errors()->add('email', 'Трябва да бъдете админ!');
            } elseif ($user && !Hash::check($request->get('password'), $user->password)) {
                $validator->errors()->add('password', 'Грешна парола');
            }
        });

        if ($validator->fails()) {
            $this->validator = $validator;
            return false;
        }

        return true;
    }

    /**
     * @param $request
     * @return AuthController
     */
    private function sendLoginResponse($request)
    {
        $request->session()->regenerate();
        auth()->login(auth()->user());

        return $this->authenticated($request, auth()->user())
            ?: $this->sendFailedLoginResponse($request);
    }

    /**
     * @param Request $request
     * @return $this
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()
            ->route('admin.loginForm')
            ->withErrors(['errors' => [
                'email' => [trans('auth.failed')]
            ]])->withInput();
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $this->validator = null;
        return redirect()->route('admin.pages.index')->with('success_message', 'Успешно се логнахте!');
    }
}
