<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\City;
use App\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $ads = Advertisement::getActiveAds();
        return view('advertisements.index', compact('ads'));
    }

    /**
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        try {
            $ad = Advertisement::where('active', true)->findOrFail($id);
            $adMainImage = $ad->images()->where('main_filename', true)->first();
            $adImages = $ad->images()->where('main_filename', false)->get();
            $ad->update(['views_count' => $ad->views_count + 1]);
        } catch (ModelNotFoundException $exception) {
            return redirect()->route('home')->with('error_message', 'Не е намерена такава обява');
        }

        $cities = City::orderBy('title', 'ASC')->get();
        $categories = Category::orderBy('title', 'ASC')->get();

        return view('advertisements.show', compact('ad', 'categories', 'cities', 'adImages', 'adMainImage'));
    }
}