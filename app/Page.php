<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        'filename',
        'slug'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return mixed
     */
    public static function getPagesWithStrippedText()
    {
        return self::orderBy('id', 'DESC')->get()->map(function ($item) {
            $item->text = mb_substr(strip_tags($item->text), 0, 100) . '...';
            return $item;
        });
    }
}
